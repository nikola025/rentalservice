To be complete, application still needs to have many things. I will just list them here, at the end.
Reason why I didn't focus on any of them is simply - time. I already spent quite a lot more than
estimated 4 hours.
My main goal was to build application what is working, according to defined requirements,
and it is complete and stable in feature it provides.
My second goal, was, as I am already spending time on this, to at learn something new.
I never used DropWizard so this was opportunity to learn it. Second experiment was to, try,
to use DDD while designing this app.
Coming from spring world I'm used to have DI available, so I choose to add Guice.
It was one hour struggle to make it work with Dropwizard and Hibernate.
Next thing was choice between some RDBMS, or memory storage implementation (either H2 or custom).
I choose postgres. Honestly it was fastest to make it work.
I supplied several prepared utility scripts to build, run, and use API.
I wanted to make usage of API as easy as possible, since it doesn't have any UI.
And it proved useful while testing in development.

And finally things I left out.
I18n. Authentication/authorization. Monitoring. API is not complete (customer and movie management).
JPA entities are not well defined (primary keys, restrictions, data types, column names, table names...).
Documentation, both JavaDoc and API documentation. Logging. Better API error handling.
Test code coverage is very low. I tested just few classes with critical business logic. No integration tests.
