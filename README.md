# RentalService

Requirements
---

1. Java 8
1. Maven 3
1. Postgres RDBMS

How to start the RentalService application
---

* Run `./build_run.sh` to build and start application.
* Application will start using default configuration, defined in file `configuration-postgres.yaml`.
* It expect database `rental` exists in DB, which can be accessed with user `postgres` and empty password.
* In case you DB has different settings please change `configuration-postgres.yaml` accordingly.

Using the API
---

Helper scripts are located in directory `scripts`. It can be used to call API methods.

* `get-all-films.sh` 		- returns all defined films with their IDs. ID need to be used when calling API to #rent film
* `get-film.sh`			- returns single film. need to pass film ID as script parameter e.g. `./get_film.sh 1`
* `post-film.sh`  		- create one film. It accept one parameter - film.json file where film details are defined. Change this file to add different films.
* `get-all-customers.sh`	- returns all defined customers with their IDs. ID need to be used when calling API to rent film
* `post-customer.sh`		- create one customer. It accept one parameter - customer.json file where customer details are defined. Change this file to add different customers.
* `post-rent-films.sh`	- rent or return a film. It accept one parameter - rent-film.json. You can change it to define film to rent using IDs of a film and customer. Or return-film.json to return a film.
*

These scripts are based on `curl`, but you can, of course, use HTTP client of your choice. Please check either given scripts or code to learn API.
