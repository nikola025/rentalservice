package com.test.rentalservice.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "new-release")
public class NewReleaseFilm extends Film {

  private static final long PREMIUM_BASE_RENT_PRICE = 40L;
  private static final int PREMIUM_BASE_RENT_DURATION_DAYS = 1;
  private static final long PREMIUM_ADDITIONAL_PRICE = 40L;

  @Override
  long getRentPricePerAdditionaltDay() {
    return PREMIUM_ADDITIONAL_PRICE;
  }

  @Override
  int getBaseRentNumberOfDays() {
    return PREMIUM_BASE_RENT_DURATION_DAYS;
  }

  @Override
  long getBaseRentPrice() {
    return PREMIUM_BASE_RENT_PRICE;
  }
}
