package com.test.rentalservice.domain.repository.dao;

import java.util.Collection;

import org.hibernate.SessionFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.RentedFilm;

import io.dropwizard.hibernate.AbstractDAO;

@Singleton
public class RentedFilmDao extends AbstractDAO<RentedFilm> {

  @Inject
  public RentedFilmDao(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  public RentedFilm findById(long id) {
    return get(id);
  }

  public long add(RentedFilm film) {
    return persist(film).getId();
  }

  public RentedFilm findByCustomerAndFilm(long customerId, long filmId) {
    return (RentedFilm) namedQuery(RentedFilm.FIND_BY_CUSTOMER_AND_FILM).setParameter("customerId", customerId)
        .setParameter("filmId", filmId).uniqueResult();
  }

  public void update(RentedFilm rentedFilm) {
    persist(rentedFilm);
  }

  public Collection<RentedFilm> findByFilm(String filmId) {
    return list(namedQuery(RentedFilm.FIND_BY_FILM).setParameter("filmId", filmId));
  }

}
