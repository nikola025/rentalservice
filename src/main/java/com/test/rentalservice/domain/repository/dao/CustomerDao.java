package com.test.rentalservice.domain.repository.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.Customer;

import io.dropwizard.hibernate.AbstractDAO;

@Singleton
public class CustomerDao extends AbstractDAO<Customer> {

  @Inject
  public CustomerDao(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  public Customer findById(long id) {
    return get(id);
  }

  public long add(Customer customer) {
    return persist(customer).getId();
  }

  public void update(Customer customer) {
    persist(customer);
  }

  public List<Customer> getAll() {
    return list(namedQuery(Customer.findAll));
  }
}
