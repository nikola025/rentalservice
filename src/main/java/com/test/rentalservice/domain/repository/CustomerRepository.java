package com.test.rentalservice.domain.repository;

import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.Customer;
import com.test.rentalservice.domain.repository.dao.CustomerDao;

@Singleton
public class CustomerRepository {

  private final CustomerDao customerDao;

  @Inject
  public CustomerRepository(CustomerDao customerDao) {
    this.customerDao = customerDao;
  }

  public Customer findById(long id) {
    return customerDao.findById(id);
  }

  public void update(Customer customer) {
    customerDao.update(customer);
  }

  public long add(Customer customer) {
    return customerDao.add(customer);
  }

  public List<Customer> getAll() {
    return customerDao.getAll();
  }

}
