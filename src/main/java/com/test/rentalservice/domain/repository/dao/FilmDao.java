package com.test.rentalservice.domain.repository.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.Film;

import io.dropwizard.hibernate.AbstractDAO;

@Singleton
public class FilmDao extends AbstractDAO<Film> {

  @Inject
  public FilmDao(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  public Film findById(long id) {
    return get(id);
  }

  public long add(Film film) {
    return persist(film).getId();
  }

  public List<Film> findAll() {
    return list(namedQuery(Film.findAll));
  }

  public void delete(long id) {
    Film film = findById(id);
    if (film != null) {
      currentSession().delete(film);
    }
  }

  public long getTotalCount() {
    return (Long) namedQuery(Film.totalCount).uniqueResult();
  }
}
