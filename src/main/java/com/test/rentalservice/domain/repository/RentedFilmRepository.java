package com.test.rentalservice.domain.repository;

import java.util.Collection;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.RentedFilm;
import com.test.rentalservice.domain.repository.dao.RentedFilmDao;

@Singleton
public class RentedFilmRepository {

  private final RentedFilmDao rentedFilmDao;

  @Inject
  public RentedFilmRepository(RentedFilmDao customerDao) {
    this.rentedFilmDao = customerDao;
  }

  public RentedFilm findById(long id) {
    return rentedFilmDao.findById(id);
  }

  public long add(RentedFilm rentedFilm) {
    return rentedFilmDao.add(rentedFilm);
  }

  public RentedFilm findByCustomerAndFilm(long customerId, long filmId) {
    return rentedFilmDao.findByCustomerAndFilm(customerId, filmId);
  }

  public void update(RentedFilm rentedFilm) {
    rentedFilmDao.update(rentedFilm);

  }

  public Collection<RentedFilm> findByFilm(String id) {
    return rentedFilmDao.findByFilm(id);
  }

}
