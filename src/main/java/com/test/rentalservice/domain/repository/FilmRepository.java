package com.test.rentalservice.domain.repository;

import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.Film;
import com.test.rentalservice.domain.repository.dao.FilmDao;

@Singleton
public class FilmRepository {

  private final FilmDao filmDao;

  @Inject
  public FilmRepository(FilmDao filmDao) {
    this.filmDao = filmDao;
  }

  public Film findById(long id) {
    return filmDao.findById(id);
  }

  public long add(Film film) {
    return filmDao.add(film);
  }

  public List<Film> getAllFilms() {
    return filmDao.findAll();
  }

  public void delete(long id) {
    filmDao.delete(id);
  }

  public long getTotalCount() {
    return filmDao.getTotalCount();
  }

}
