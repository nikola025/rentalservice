package com.test.rentalservice.domain;

import static java.lang.String.valueOf;

import java.util.List;
import java.util.stream.Collectors;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.infrastructure.CustomerFactory;
import com.test.rentalservice.domain.infrastructure.CustomerVO;
import com.test.rentalservice.domain.repository.CustomerRepository;

@Singleton
public class CustomerService {

  private final CustomerRepository customerRepository;

  @Inject
  public CustomerService(CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }

  public String addCustomer(CustomerVO customer) {
    return valueOf(customerRepository.add(CustomerFactory.createFromVo(customer)));
  }

  public List<CustomerVO> getAllCustomers() {
    return customerRepository.getAll().stream().map(this::toCustomerVO).collect(Collectors.toList());
  }

  private CustomerVO toCustomerVO(Customer customer) {
    CustomerVO customerVO = new CustomerVO();
    customerVO.setId(valueOf(customer.getId()));
    customerVO.setFirstName(customer.getFirstName());
    customerVO.setLastName(customer.getLastName());
    return customerVO;
  }
}
