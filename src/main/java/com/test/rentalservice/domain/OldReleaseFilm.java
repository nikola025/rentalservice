package com.test.rentalservice.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "old-release")
public class OldReleaseFilm extends Film {

  private static final int OLD_BASE_RENT_PRICE = 30;
  private static final int OLD_BASE_RENT_DURATION_DAYS = 5;
  private static final int OLD_ADDITIONAL_PRICE = 30;

  @Override
  long getRentPricePerAdditionaltDay() {
    return OLD_ADDITIONAL_PRICE;
  }

  @Override
  int getBaseRentNumberOfDays() {
    return OLD_BASE_RENT_DURATION_DAYS;
  }

  @Override
  long getBaseRentPrice() {
    return OLD_BASE_RENT_PRICE;
  }
}
