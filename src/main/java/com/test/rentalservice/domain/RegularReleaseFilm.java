package com.test.rentalservice.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "regular-release")
public class RegularReleaseFilm extends Film {

  private static final long REGULAR_BASE_RENT_PRICE = 30L;
  private static final int REGULAR_BASE_RENT_DURATION_DAYS = 3;
  private static final long REGULAR_ADDITIONAL_PRICE = 30L;

  @Override
  long getRentPricePerAdditionaltDay() {
    return REGULAR_ADDITIONAL_PRICE;
  }

  @Override
  int getBaseRentNumberOfDays() {
    return REGULAR_BASE_RENT_DURATION_DAYS;
  }

  @Override
  long getBaseRentPrice() {
    return REGULAR_BASE_RENT_PRICE;
  }
}
