package com.test.rentalservice.domain;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.infrastructure.FilmFactory;
import com.test.rentalservice.domain.infrastructure.FilmVO;
import com.test.rentalservice.domain.infrastructure.ReleaseType;
import com.test.rentalservice.domain.repository.FilmRepository;
import com.test.rentalservice.domain.repository.RentedFilmRepository;

@Singleton
public class FilmService {

  private final FilmRepository filmRepository;
  private final RentedFilmRepository rentedFilmRepository;

  @Inject
  public FilmService(FilmRepository filmRepository, RentedFilmRepository rentedFilmRepository) {
    this.filmRepository = filmRepository;
    this.rentedFilmRepository = rentedFilmRepository;
  }

  public List<FilmVO> getAllFilms() {
    return filmRepository.getAllFilms().stream().map(FilmService::toFilmVO).collect(Collectors.toList());
  }

  public long getTotalNumberOfFilms() {
    return filmRepository.getTotalCount();
  }

  public FilmVO findFilm(String filmId) {
    return toFilmVO(filmRepository.findById(Long.parseLong(filmId)));
  }

  private static FilmVO toFilmVO(Film film) {
    return new FilmVO(String.valueOf(film.getId()), film.getTitle(), ReleaseType.toReleaseType(film),
        String.valueOf(film.getBaseRentPrice()), String.valueOf(film.getBaseRentNumberOfDays()),
        String.valueOf(film.getRentPricePerAdditionaltDay()));
  }

  public String addFilm(FilmVO film) {
    return String.valueOf(filmRepository.add(FilmFactory.createFromVo(film)));
  }

  public boolean deleteFilm(String id) {
    // Delete is possible only if film is not rented
    if (CollectionUtils.isEmpty(rentedFilmRepository.findByFilm(id))) {
      filmRepository.delete(Long.parseLong(id));
      return true;
    }
    return false;
  }
}
