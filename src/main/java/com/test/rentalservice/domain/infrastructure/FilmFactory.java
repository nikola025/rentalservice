package com.test.rentalservice.domain.infrastructure;

import com.test.rentalservice.domain.Film;

public class FilmFactory {

  public static Film createFromVo(FilmVO filmVO) {
    Film film = ReleaseType.fromReleaseType(filmVO.getReleaseType());
    film.setTitle(filmVO.getTitle());
    return film;
  }

}
