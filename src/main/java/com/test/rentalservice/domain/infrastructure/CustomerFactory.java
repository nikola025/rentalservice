package com.test.rentalservice.domain.infrastructure;

import com.test.rentalservice.domain.Customer;

public class CustomerFactory {

  public static Customer createFromVo(CustomerVO customerVO) {
    Customer customer = new Customer();
    customer.setFirstName(customerVO.getFirstName());
    customer.setLastName(customerVO.getLastName());
    return customer;
  }

}
