package com.test.rentalservice.domain.infrastructure;

import javax.validation.constraints.NotNull;

public class FilmVO {

  private String id;

  @NotNull
  private String title;

  @NotNull
  private ReleaseType releaseType;

  private String baseRentingPrice;

  private String baseRentingDuration;

  private String additionalPricePerDay;

  public FilmVO(String id, String title, ReleaseType releaseType, String baseRentingPrice, String baseRentingDuration,
      String additionalPricePerDay) {
    this.id = id;
    this.title = title;
    this.releaseType = releaseType;
    this.baseRentingPrice = baseRentingPrice;
    this.baseRentingDuration = baseRentingDuration;
    this.additionalPricePerDay = additionalPricePerDay;
  }

  public FilmVO() {
  }

  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public ReleaseType getReleaseType() {
    return releaseType;
  }

  public String getBaseRentingPrice() {
    return baseRentingPrice;
  }

  public String getBaseRentingDuration() {
    return baseRentingDuration;
  }

  public String getAdditionalPricePerDay() {
    return additionalPricePerDay;
  }

  @Override
  public String toString() {
    return "FilmVO [id=" + id + ", name=" + title + ", releaseType=" + releaseType + ", baseRentingPrice="
        + baseRentingPrice + ", baseRentingDuration=" + baseRentingDuration + ", additionalPricePerDay="
        + additionalPricePerDay + "]";
  }
}
