package com.test.rentalservice.domain.infrastructure;

import java.util.List;

import javax.validation.constraints.NotNull;

public class RentFilmsRequestVO {

  @NotNull
  private String action;

  @NotNull
  private List<RentFilmVO> films;

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public List<RentFilmVO> getFilms() {
    return films;
  }

  public void setFilms(List<RentFilmVO> films) {
    this.films = films;
  }

}
