package com.test.rentalservice.domain.infrastructure;

import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;

import com.test.rentalservice.domain.Customer;
import com.test.rentalservice.domain.Film;
import com.test.rentalservice.domain.RentedFilm;

public class RentedFilmBuilder {

  private final RentedFilm instance;

  public RentedFilmBuilder() {
    instance = new RentedFilm();
  }

  public RentedFilmBuilder film(Film film) {
    instance.setFilm(film);
    return this;
  }

  public RentedFilmBuilder customer(Customer customer) {
    instance.setCustomer(customer);
    return this;
  }

  public RentedFilmBuilder days(int days) {
    instance.setNumberOfDaysRented(days);
    return this;
  }

  public RentedFilm build() {
    Validate.notNull(instance.getFilm(), "Film must not be null!");
    Validate.notNull(instance.getCustomer(), "Customer must not be null!");
    Validate.isTrue(instance.getNumberOfDaysRented() > 0, "Number of days must be > 0");
    instance.setRentStartDate(DateTime.now());
    instance.setReturned(false);
    return instance;
  }
}
