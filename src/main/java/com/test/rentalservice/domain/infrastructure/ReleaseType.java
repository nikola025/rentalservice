package com.test.rentalservice.domain.infrastructure;

import com.test.rentalservice.domain.Film;
import com.test.rentalservice.domain.NewReleaseFilm;
import com.test.rentalservice.domain.OldReleaseFilm;
import com.test.rentalservice.domain.RegularReleaseFilm;

public enum ReleaseType {

  PREMIUM_RELEASE,
  REGULAR_RELEASE,
  OLD_RELEASE;

  public static Film fromReleaseType(ReleaseType releaseType) {
    switch (releaseType) {
      case PREMIUM_RELEASE:
        return new NewReleaseFilm();
      case REGULAR_RELEASE:
        return new RegularReleaseFilm();
      case OLD_RELEASE:
        return new OldReleaseFilm();
      default:
        throw new IllegalStateException("Unknown relase type!");
    }
  }

  public static ReleaseType toReleaseType(Film film) {
    if (film instanceof NewReleaseFilm) {
      return ReleaseType.PREMIUM_RELEASE;
    } else if (film instanceof RegularReleaseFilm) {
      return ReleaseType.REGULAR_RELEASE;
    } else if (film instanceof OldReleaseFilm) {
      return ReleaseType.OLD_RELEASE;
    } else {
      throw new IllegalStateException("Unknown relase type!");
    }
  }
}
