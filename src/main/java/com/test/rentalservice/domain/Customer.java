package com.test.rentalservice.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
@NamedQueries({ @NamedQuery(name = Customer.findAll, query = Customer.findAllQuery) })
public class Customer {

  public static final String findAll = "com.test.rentalservice.domain.Customer.findAll";
  protected static final String findAllQuery = "select c from Customer c";

  @Id
  private long id;

  private String firstName;

  private String lastName;

  private long bonusPoints;

  public void addBonusPoints(long points) {
    this.bonusPoints += points;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public long getBonusPoints() {
    return bonusPoints;
  }

  public void setBonusPoints(long bonusPoints) {
    this.bonusPoints = bonusPoints;
  }

  @Override
  public String toString() {
    return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", bonusPoints=" + bonusPoints
        + "]";
  }
}
