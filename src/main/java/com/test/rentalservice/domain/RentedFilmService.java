package com.test.rentalservice.domain;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.infrastructure.RentFilmVO;
import com.test.rentalservice.domain.infrastructure.RentedFilmBuilder;
import com.test.rentalservice.domain.repository.CustomerRepository;
import com.test.rentalservice.domain.repository.FilmRepository;
import com.test.rentalservice.domain.repository.RentedFilmRepository;

@Singleton
public class RentedFilmService {

  private final FilmRepository filmRepository;
  private final CustomerRepository customerRepository;
  private final RentedFilmRepository rentedFilmRepository;

  @Inject
  public RentedFilmService(FilmRepository filmRepository, CustomerRepository customerRepository,
      RentedFilmRepository rentedFilmRepository) {
    this.filmRepository = filmRepository;
    this.customerRepository = customerRepository;
    this.rentedFilmRepository = rentedFilmRepository;
  }

  /**
   * Method will rent all films for given list, calculate rent price for each film based on number of days and film
   * renting price. For each film customer will earn bonus points which will be added to his account. Same film will not
   * be rented twice. If it is already rented by this customer request will be ignored.
   * 
   * @param films
   *          - the films to rent
   * @return sum of prices for each rented film
   */
  public long rentFilms(List<RentFilmVO> films) {
    List<Long> filmPrices = rentFilmsAndCalculatePrices(films);
    addBonusPointsToCustomer(films.get(0).getCustomerId(), filmPrices.size());
    return sum(filmPrices);
  }

  /**
   * End renting (return films) for each film from the given list. In case film is not rented request will be ignored.
   * For each film penalty price will be calculated, based on return date.
   * 
   * @param films
   *          - films to return
   * @return sum of all penalty prices for each returned film
   */
  public long returnFilms(List<RentFilmVO> films) {
    return films.stream().mapToLong(this::returnAFilmAndCalculatePenaltyPrice).sum();
  }

  private List<Long> rentFilmsAndCalculatePrices(List<RentFilmVO> films) {
    return films.stream().filter(this::filmExist).map(this::rentAFilmAndCalculatePrice).collect(Collectors.toList());
  }

  private long rentAFilmAndCalculatePrice(RentFilmVO filmToRent) {
    if (isFilmAlreadyRented(filmToRent)) {
      // Customer already rented this film
      return 0;
    }
    RentedFilm rentedFilm = new RentedFilmBuilder().customer(loadCustomer(parseLong(filmToRent.getCustomerId())))
        .film(loadFilm(filmToRent)).days(parseInt(filmToRent.getDays())).build();
    rentedFilmRepository.add(rentedFilm);
    // calculate price and return it
    return rentedFilm.calculateRentPrice();
  }

  private long returnAFilmAndCalculatePenaltyPrice(RentFilmVO filmToRent) {
    // load RentedFilm from DB based on customer and film, take into account "returned" field
    RentedFilm rentedFilm = findRentedFilmForCustomer(filmToRent.getId(), filmToRent.getCustomerId());
    if (isFilmRented(rentedFilm)) {
      // end rent (update date), and returned field, and store it to DB
      rentedFilm.setRentEndDate(DateTime.now());
      rentedFilm.setReturned(true);
      rentedFilmRepository.update(rentedFilm);
      // calculate it's penalty price
      return rentedFilm.calculatePenaltyPrice();
    }
    return 0;
  }

  private void addBonusPointsToCustomer(String customerId, long points) {
    Customer customer = customerRepository.findById(parseLong(customerId));
    customer.addBonusPoints(points);
    customerRepository.update(customer);
  }

  private boolean filmExist(RentFilmVO filmToRent) {
    return loadFilm(filmToRent) != null;
  }

  private boolean isFilmAlreadyRented(RentFilmVO filmToRent) {
    return findRentedFilmForCustomer(filmToRent.getId(), filmToRent.getCustomerId()) != null;
  }

  private RentedFilm findRentedFilmForCustomer(String id, String customerId) {
    return rentedFilmRepository.findByCustomerAndFilm(parseLong(customerId), parseLong(id));
  }

  private Customer loadCustomer(long id) {
    return customerRepository.findById(id);
  }

  private Film loadFilm(RentFilmVO filmToRent) {
    return filmRepository.findById(parseLong(filmToRent.getId()));
  }

  private static boolean isFilmRented(RentedFilm rentedFilm) {
    // check it exist (film is rented) and it is not returned
    return rentedFilm != null && !rentedFilm.isReturned();
  }

  private static long sum(List<Long> list) {
    return list.stream().mapToLong(Long::longValue).sum();
  }

}
