package com.test.rentalservice.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({ @NamedQuery(name = Film.findAll, query = Film.findAllQuery),
    @NamedQuery(name = Film.totalCount, query = Film.totalCountQuery) })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "release_type")
@Entity
@Table(name = "film")
public abstract class Film {

  public static final String totalCount = "com.test.rentalservice.domain.Film.totalCount";
  protected static final String totalCountQuery = "select count(*) from Film";

  public static final String findAll = "com.test.rentalservice.domain.Film.findAll";
  protected static final String findAllQuery = "select f from Film f";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  long id;

  @Column(name = "title")
  String title;

  long calculateRentPrice(int numberOfDays) {
    return getBaseRentPrice() + calculateAdditionalRentPrice(numberOfDays);
  }

  private long calculateAdditionalRentPrice(int numberOfDays) {
    int numberOfAdditionalDays = numberOfDays - getBaseRentNumberOfDays();
    if (numberOfAdditionalDays > 0) {
      return numberOfAdditionalDays * getRentPricePerAdditionaltDay();
    }
    return 0;
  }

  abstract long getRentPricePerAdditionaltDay();

  abstract int getBaseRentNumberOfDays();

  abstract long getBaseRentPrice();

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

}
