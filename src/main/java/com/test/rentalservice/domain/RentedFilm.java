package com.test.rentalservice.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.joda.time.DateTime;
import org.joda.time.Days;

@Entity
@Table(name = "rented_film")
@NamedQueries({
    @NamedQuery(name = RentedFilm.FIND_BY_CUSTOMER_AND_FILM, query = RentedFilm.FIND_BY_CUSTOMER_AND_FILM_QUERY),
    @NamedQuery(name = RentedFilm.FIND_BY_FILM, query = RentedFilm.FIND_BY_FILM_QUERY) })
public class RentedFilm {

  public static final String FIND_BY_CUSTOMER_AND_FILM =
      "com.test.rentalservice.domain.RentedFilm.findByCustomerAndFilm";
  public static final String FIND_BY_CUSTOMER_AND_FILM_QUERY =
      "select r from RentedFilm r where r.customer.id = :customerId and r.film.id = :filmId and returned = false";

  public static final String FIND_BY_FILM = "com.test.rentalservice.domain.RentedFilm.findByFilm";
  public static final String FIND_BY_FILM_QUERY = "select r from RentedFilm r where r.film.id = :filmId";

  @Id
  @GeneratedValue
  private long id;

  private boolean returned;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "customerid", nullable = false)
  private Customer customer;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "filmid", nullable = false)
  private Film film;

  private DateTime rentStartDate;

  private int numberOfDaysRented;

  private DateTime rentEndDate;

  public long calculateRentPrice() {
    return film.calculateRentPrice(numberOfDaysRented);
  }

  public long calculatePenaltyPrice() {
    int actualNumberOfRentedDays = Days.daysBetween(rentStartDate, rentEndDate).getDays();
    if (customerIsLate(actualNumberOfRentedDays)) {
      return film.calculateRentPrice(actualNumberOfRentedDays) - film.calculateRentPrice(numberOfDaysRented);
    }
    return 0;
  }

  private boolean customerIsLate(int actualNumberOfRentedDays) {
    return actualNumberOfRentedDays > numberOfDaysRented;
  }

  public boolean isReturned() {
    return returned;
  }

  public void setReturned(boolean returned) {
    this.returned = returned;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Film getFilm() {
    return film;
  }

  public void setFilm(Film film) {
    this.film = film;
  }

  public DateTime getRentStartDate() {
    return rentStartDate;
  }

  public void setRentStartDate(DateTime rentStartDate) {
    this.rentStartDate = rentStartDate;
  }

  public int getNumberOfDaysRented() {
    return numberOfDaysRented;
  }

  public void setNumberOfDaysRented(int numberOfDaysRented) {
    this.numberOfDaysRented = numberOfDaysRented;
  }

  public DateTime getRentEndDate() {
    return rentEndDate;
  }

  public void setRentEndDate(DateTime rentEndDate) {
    this.rentEndDate = rentEndDate;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

}
