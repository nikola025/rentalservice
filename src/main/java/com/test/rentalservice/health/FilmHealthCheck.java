package com.test.rentalservice.health;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.test.rentalservice.domain.FilmService;

@Singleton
public class FilmHealthCheck extends HealthCheck {

  public static final String MONITOR_NAME = "filmHealthCheck";
  private final FilmService filmService;

  @Inject
  public FilmHealthCheck(FilmService filmService) {
    this.filmService = filmService;
  }

  @Override
  protected Result check() throws Exception {
    if (isFilmRepositoryEmpty()) {
      return Result.unhealthy("No films in DB, something is terribly wrong!");
    }
    return Result.healthy();
  }

  private boolean isFilmRepositoryEmpty() {
    return filmService.getTotalNumberOfFilms() == 0;
  }

}
