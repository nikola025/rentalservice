package com.test.rentalservice.resources;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;
import com.test.rentalservice.domain.RentedFilmService;
import com.test.rentalservice.domain.infrastructure.RentFilmVO;
import com.test.rentalservice.domain.infrastructure.RentFilmsRequestVO;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/rent-films")
@Produces(MediaType.APPLICATION_JSON)
public class RentFilmsResource {

  private static final String ACTION_RENT = "rent-films";
  private static final Object ACTION_RETURN = "return-films";
  private final RentedFilmService rentedFilmService;

  @Inject
  public RentFilmsResource(RentedFilmService rentedFilmService) {
    this.rentedFilmService = rentedFilmService;
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @UnitOfWork
  public Response rentFilms(@Valid RentFilmsRequestVO rentRequest) {
    if (StringUtils.isBlank(rentRequest.getAction())) {
      return badRequestNoActionParam();
    }
    if (CollectionUtils.isEmpty(rentRequest.getFilms())) {
      return badRequestNoFilmsParam();
    }
    if (ACTION_RENT.equals(rentRequest.getAction())) {
      return rentFilms(rentRequest.getFilms());
    } else if (ACTION_RETURN.equals(rentRequest.getAction())) {
      return returnFilms(rentRequest.getFilms());
    } else {
      return badRequestNoActionParam();
    }
  }

  private Response returnFilms(List<RentFilmVO> films) {
    long price = rentedFilmService.returnFilms(films);
    return Response.ok(price).build();
  }

  private Response rentFilms(List<RentFilmVO> films) {
    long price = rentedFilmService.rentFilms(films);
    return Response.ok(price).build();
  }

  private Response badRequestNoFilmsParam() {
    return Response.status(Status.BAD_REQUEST).entity("Request must contain correct films param!").build();
  }

  private Response badRequestNoActionParam() {
    return Response.status(Status.BAD_REQUEST).entity("Request must contain correct action (rent-films/return-films)!")
        .build();
  }

}
