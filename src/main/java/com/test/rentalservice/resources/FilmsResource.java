package com.test.rentalservice.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.test.rentalservice.domain.FilmService;
import com.test.rentalservice.domain.infrastructure.FilmVO;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/films")
@Produces(MediaType.APPLICATION_JSON)
public class FilmsResource {

  private final FilmService filmService;

  @Inject
  public FilmsResource(FilmService filmService) {
    this.filmService = filmService;
  }

  @GET
  @UnitOfWork
  public List<FilmVO> getAllFilms() {
    return filmService.getAllFilms();
  }
}
