package com.test.rentalservice.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.test.rentalservice.domain.CustomerService;
import com.test.rentalservice.domain.infrastructure.CustomerVO;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/customers")
@Produces(MediaType.APPLICATION_JSON)
public class CustomersResource {

  private final CustomerService customerService;

  @Inject
  public CustomersResource(CustomerService customerService) {
    this.customerService = customerService;
  }

  @GET
  @UnitOfWork
  public List<CustomerVO> getAllCustomers() {
    return customerService.getAllCustomers();
  }
}
