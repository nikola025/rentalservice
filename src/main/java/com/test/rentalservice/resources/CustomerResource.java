package com.test.rentalservice.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.test.rentalservice.domain.CustomerService;
import com.test.rentalservice.domain.infrastructure.CustomerVO;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/customer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {

  private final CustomerService customerService;

  @Inject
  public CustomerResource(CustomerService customerService) {
    this.customerService = customerService;
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @UnitOfWork
  public String addCustomer(@NotNull @Valid CustomerVO customer) {
    return customerService.addCustomer(customer);
  }

}
