package com.test.rentalservice.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.inject.Inject;
import com.test.rentalservice.domain.FilmService;
import com.test.rentalservice.domain.infrastructure.FilmVO;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/film")
@Produces(MediaType.APPLICATION_JSON)
public class FilmResource {

  private final String LONG_PATTERN = "^-?\\d{1,19}$";

  private final FilmService filmService;

  @Inject
  public FilmResource(FilmService filmService) {
    this.filmService = filmService;
  }

  @GET
  @Path("{id}")
  @UnitOfWork
  public FilmVO findFilm(@PathParam("id") @Pattern(regexp = LONG_PATTERN) @Valid String filmId) {
    return filmService.findFilm(filmId);
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @UnitOfWork
  public String addFilm(@NotNull @Valid FilmVO film) {
    return filmService.addFilm(film);
  }

  @DELETE
  @Path("{id}")
  @UnitOfWork
  public Response deleteFilm(@PathParam("id") @Pattern(regexp = LONG_PATTERN) @Valid String id) {
    if (filmService.deleteFilm(id)) {
      return Response.noContent().build();
    } else {
      return Response.status(Status.FORBIDDEN).entity("Not allowed to delete film, it is rented currently.").build();
    }
  }

}
