package com.test.rentalservice.application;

import org.hibernate.SessionFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.ProvisionException;

public class RentalServiceModule extends AbstractModule {

  private SessionFactory sessionFactory;

  @Override
  protected void configure() {
  }

  @Provides
  SessionFactory providesSessionFactory() {
    if (sessionFactory == null) {
      throw new ProvisionException(
          "The Hibernate session factory has not yet been set. This is likely caused by forgetting to call setSessionFactory during Application.run()");
    }
    return sessionFactory;
  }

  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }
}
