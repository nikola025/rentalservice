package com.test.rentalservice.application;

import com.hubspot.dropwizard.guice.GuiceBundle;
import com.test.rentalservice.domain.Customer;
import com.test.rentalservice.domain.Film;
import com.test.rentalservice.domain.NewReleaseFilm;
import com.test.rentalservice.domain.OldReleaseFilm;
import com.test.rentalservice.domain.RegularReleaseFilm;
import com.test.rentalservice.domain.RentedFilm;
import com.test.rentalservice.health.FilmHealthCheck;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class RentalServiceApplication extends Application<RentalServiceConfiguration> {

  private GuiceBundle<RentalServiceConfiguration> guiceBundle;

  private RentalServiceModule rentalServiceModule;

  public static void main(final String[] args) throws Exception {
    new RentalServiceApplication().run(args);
  }

  @Override
  public String getName() {
    return "RentalService";
  }

  private final HibernateBundle<RentalServiceConfiguration> hibernate =
      new HibernateBundle<RentalServiceConfiguration>(Film.class, NewReleaseFilm.class, RegularReleaseFilm.class,
          OldReleaseFilm.class, Customer.class, RentedFilm.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(RentalServiceConfiguration configuration) {
          return configuration.getDataSourceFactory();
        }
      };

  @Override
  public void initialize(final Bootstrap<RentalServiceConfiguration> bootstrap) {
    rentalServiceModule = new RentalServiceModule();
    guiceBundle = GuiceBundle.<RentalServiceConfiguration> newBuilder().addModule(rentalServiceModule)
        .enableAutoConfig("com.test.rentalservice").setConfigClass(RentalServiceConfiguration.class).build();

    bootstrap.addBundle(guiceBundle);
    bootstrap.addBundle(hibernate);
  }

  @Override
  public void run(final RentalServiceConfiguration configuration, final Environment environment) {
    rentalServiceModule.setSessionFactory(hibernate.getSessionFactory());
    final FilmHealthCheck healthCheck = guiceBundle.getInjector().getInstance(FilmHealthCheck.class);
    environment.healthChecks().register(FilmHealthCheck.MONITOR_NAME, healthCheck);
  }

}
