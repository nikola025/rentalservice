package com.test.rentalservice.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;

public class FilmTest {

  @Test
  public void testNewReleaseFilmRentPrice() {
    NewReleaseFilm film = new NewReleaseFilm();
    assertThat(film.calculateRentPrice(1), is(40L));
    assertThat(film.calculateRentPrice(2), is(80L));
    assertThat(film.calculateRentPrice(3), is(120L));
  }

  @Test
  public void testRegularReleaseFilmRentPrice() {
    RegularReleaseFilm film = new RegularReleaseFilm();
    assertThat(film.calculateRentPrice(1), is(30L));
    assertThat(film.calculateRentPrice(3), is(30L));
    assertThat(film.calculateRentPrice(5), is(90L));
  }

  @Test
  public void testOldReleaseFilmRentPrice() {
    OldReleaseFilm film = new OldReleaseFilm();
    assertThat(film.calculateRentPrice(1), is(30L));
    assertThat(film.calculateRentPrice(5), is(30L));
    assertThat(film.calculateRentPrice(8), is(120L));
  }
}
