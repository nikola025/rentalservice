package com.test.rentalservice.domain;

import static java.lang.Long.parseLong;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.google.common.collect.Lists;
import com.test.rentalservice.domain.infrastructure.RentFilmVO;
import com.test.rentalservice.domain.repository.CustomerRepository;
import com.test.rentalservice.domain.repository.FilmRepository;
import com.test.rentalservice.domain.repository.RentedFilmRepository;

public class RentedFilmServiceTest {

  private static final String CUSTOMER_ID = "1";
  private static final String FILM_ID = "2";
  private static final String DAYS = "3";

  @Mock
  private FilmRepository filmRepository;
  @Mock
  private CustomerRepository customerRepository;
  @Mock
  private RentedFilmRepository rentedFilmRepository;

  private RentedFilmService testee;
  @Mock
  private Customer customer;
  @Mock
  private RentedFilm rentedFilm;

  private OldReleaseFilm film;

  @Before
  public void setup() {
    initMocks(this);
    testee = new RentedFilmService(filmRepository, customerRepository, rentedFilmRepository);
    film = new OldReleaseFilm();

    when(customerRepository.findById(parseLong(CUSTOMER_ID))).thenReturn(customer);
    when(filmRepository.findById(parseLong(FILM_ID))).thenReturn(film);
  }

  @Test
  public void shouldRentAFilm() {
    Long price = testee.rentFilms(createFilms());
    // Verify return value
    assertThat(price.longValue(), is(30l));

    // Verify RentedFilm entity stored
    ArgumentCaptor<RentedFilm> rentedFilmCaptor = ArgumentCaptor.forClass(RentedFilm.class);
    verify(rentedFilmRepository, times(1)).add(rentedFilmCaptor.capture());
    assertThat(rentedFilmCaptor.getValue().getFilm(), is(film));
    assertThat(rentedFilmCaptor.getValue().getCustomer(), is(customer));
    assertThat(rentedFilmCaptor.getValue().getNumberOfDaysRented(), is(3));

    // Verify bonus point added
    verify(customer).addBonusPoints(1l);
    verify(customerRepository).update(customer);
  }

  @Test
  public void shouldReturnAFilm() {
    when(rentedFilmRepository.findByCustomerAndFilm(parseLong(CUSTOMER_ID), parseLong(FILM_ID))).thenReturn(rentedFilm);

    Long price = testee.returnFilms(createFilms());
    // Verify return value
    assertThat(price.longValue(), is(0l));

    // Verify RentedFilm entity is updated
    verify(rentedFilm).setReturned(true);
    verify(rentedFilm).setRentEndDate(Mockito.any(DateTime.class));
    verify(rentedFilmRepository).update(rentedFilm);
  }

  private List<RentFilmVO> createFilms() {
    RentFilmVO rentFilmVO = new RentFilmVO();
    rentFilmVO.setCustomerId(CUSTOMER_ID);
    rentFilmVO.setId(FILM_ID);
    rentFilmVO.setDays(DAYS);
    return Lists.newArrayList(rentFilmVO);
  }

}
