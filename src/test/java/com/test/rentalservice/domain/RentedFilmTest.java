package com.test.rentalservice.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.joda.time.DateTime;
import org.junit.Test;

public class RentedFilmTest {

  @Test
  public void testCalculateRentPriceForPremiumFilm() {
    // 1 day = 40SEK
    RentedFilm rentedFilm = createPremiumRentedFilm(1, 1);
    assertThat(rentedFilm.calculateRentPrice(), is(40L));
    assertThat(rentedFilm.calculatePenaltyPrice(), is(0L));

    // 3 days = 40 + 80
    RentedFilm latePremiumFilm = createPremiumRentedFilm(1, 3);
    assertThat(latePremiumFilm.calculateRentPrice(), is(40L));
    assertThat(latePremiumFilm.calculatePenaltyPrice(), is(80L));
  }

  @Test
  public void testCalculateRentPriceForRegularFilm() {
    // 5 day = 90SEK (30 + 2 additional days * 30)
    RentedFilm rentedFilm = createRegularRentedFilm(5, 5);
    assertThat(rentedFilm.calculateRentPrice(), is(90L));
    assertThat(rentedFilm.calculatePenaltyPrice(), is(0L));

    // 2 days = 30SEK (base price = 30)
    rentedFilm = createRegularRentedFilm(2, 2);
    assertThat(rentedFilm.calculateRentPrice(), is(30L));
    assertThat(rentedFilm.calculatePenaltyPrice(), is(0L));

    // 2 days = 30K
    rentedFilm = createRegularRentedFilm(2, 3);
    assertThat(rentedFilm.calculateRentPrice(), is(30L));
    // additional day is not charged b/c it fits into base period of 3 days
    assertThat(rentedFilm.calculatePenaltyPrice(), is(0L));
  }

  @Test
  public void testOldReleaseFilm() {
    // 7 days = 30 + 2*30 = 90SEK
    RentedFilm oldRentedFilm = createOldRentedFilm(7, 7);
    assertThat(oldRentedFilm.calculateRentPrice(), is(90L));
    assertThat(oldRentedFilm.calculatePenaltyPrice(), is(0L));

    // 9 days = 5 days (30SEK) + 4*30 = 150
    oldRentedFilm = createOldRentedFilm(7, 9);
    // initially payed 90
    assertThat(oldRentedFilm.calculateRentPrice(), is(90L));
    // additional charge of 60 for remaining 2 days
    assertThat(oldRentedFilm.calculatePenaltyPrice(), is(60L));
  }

  private RentedFilm createPremiumRentedFilm(int daysRented, int actualDaysRented) {
    return createRentedFile(new NewReleaseFilm(), daysRented, actualDaysRented);
  }

  private RentedFilm createRegularRentedFilm(int daysRented, int actualDaysRented) {
    return createRentedFile(new RegularReleaseFilm(), daysRented, actualDaysRented);
  }

  private RentedFilm createOldRentedFilm(int daysRented, int actualDaysRented) {
    return createRentedFile(new OldReleaseFilm(), daysRented, actualDaysRented);
  }

  private RentedFilm createRentedFile(Film film, int rentDays, int actualRentDays) {
    RentedFilm rentedFilm = new RentedFilm();
    rentedFilm.setFilm(film);
    rentedFilm.setNumberOfDaysRented(rentDays);
    DateTime now = DateTime.now();
    rentedFilm.setRentStartDate(now);
    rentedFilm.setRentEndDate(now.plusDays(actualRentDays));
    return rentedFilm;
  }
}
